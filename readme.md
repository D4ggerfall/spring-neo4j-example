##This is the final project for DBI in the year 2017/18.

###What this project is about:

This project is a representation of chefs, their accomplices, recipes and restaurants.

This is realized using Spring Boot with Neo4J as a database.


###Requirements

A local running Neo4J database, preferably as a docker container.
